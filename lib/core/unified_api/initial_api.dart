import 'dart:convert';
import 'dart:io';
import '../constant/apis/apis_url.dart';
import 'handling_response.dart';
import 'printing.dart';

abstract class InitialApi<T> extends Printing with HandlingResponse {
  InitialApi({required this.url, required String requestName, this.header})
      : super(requestName: requestName) {
    header ??= {
      HttpHeaders.contentTypeHeader: "application/json",
      HttpHeaders.acceptHeader: "application/json",
    };
  }
  String? url;
  final String baseURL = ApiUrls.host;
  Map<String, String>? header;

  Future<T> callRequest();
}
