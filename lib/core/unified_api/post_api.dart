import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import '../constant/apis/apis_url.dart';
import 'printing.dart';

import 'initial_api.dart';
import 'package:http/http.dart' as http;

typedef _FromJson<T> = T Function(String body);

class PostApi<T> extends InitialApi<T> {
  PostApi({
    required String url,
    this.body,
    required this.fromJson,
    required String requestName,
    Map<String, String>? header,
  }) : super(requestName: requestName, url: url, header: header);

  Map<String, dynamic>? body;
  _FromJson<T> fromJson;
  @override
  Future<T> callRequest() async {
    final Uri uri = Uri(
      scheme: 'https',
      host: ApiUrls.host,
      path: url,
    );
    printRequest(
      requestType: RequestType.post,
      uri: uri,
      param: body,
    );
    final http.Response response = await http
        .post(
          uri,
          headers: header,
          body: body,
        )
        .timeout(
          const Duration(seconds: 30),
        );

    printResponse(response);

    if (response.statusCode == 200) {
      log('hhhh');
      return fromJson(response.body);
    } else {
      Exception exception = getException(statusCode: response.statusCode);
      throw (exception);
    }
  }
}
