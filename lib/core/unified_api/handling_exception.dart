import 'dart:async';
import 'dart:developer';
import 'package:dartz/dartz.dart';
import '../error/exception.dart';
import '../error/failures.dart';


class HandlingExceptionManager<T>{
  
  static Future<Either<Failure, T>> wrapHandling<T>(
      {required Future<Right<Failure,T>> Function() tryCall }) async {
    try {
      final right = await tryCall();
      return right;
    } on ServerException {
      log("<< ServerException >> ");
      return Left(ServerFailure());
    } on DataDuplicatesException {
      log("<< DataDuplicatesException >> ");
      return Left(DataDuplicatesFailure());
    } on MissingParamException {
      log("<< MissingParamException >> ");
      return Left(MissingParamFailure());
    } on UserNotAllowedToAccessException {
      log("<< UserNotAllowedToAccessException >> ");
      return Left(UserNotAllowedToAccessFailure());
    } on OperationFailedException {
      log("<< OperationFailedException >> ");
      return Left(OperationFailedFailure());
    } on TokenMisMatchException {
      log("<< TokenMisMatchException >> ");
      return Left(TokenMisMatchFailure());
    } on DataNotFoundException {
      log("<< DataNotFoundException >> ");
      return Left(DataNotFoundFailure());
    } on InvalidEmailException {
      log("<< InvalidEmailException >> ");
      return Left(InvalidEmailFailure());
    } on InvalidPhoneException {
      log("<< InvalidPhoneException >> ");
      return Left(InvalidPhoneFailure());
    } on NotAuthenticatedException {
      log("<< NotAuthenticatedException >> ");
      return Left(NotAuthenticatedFailure());
    } on TimeoutException {
      log("<< TimeoutException >> ");
      return Left(TimeOutFailure());
    } catch (e) {
      log("<< catch >> error is $e");
      return Left(ServerFailure());
    }
  }
}
