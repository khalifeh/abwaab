import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import '../constant/apis/apis_url.dart';
import 'printing.dart';
import 'package:http/http.dart' as http;
import 'initial_api.dart';

typedef _FromJson<T> = T Function(String body);

class PutApi<T> extends InitialApi<T> {
  PutApi({
    required String url,
    required this.param,
    required this.fromJson,
    required String requestName,
  }) : super(requestName: requestName, url: url);

  Map<String, dynamic> param;
  _FromJson<T> fromJson;

  @override
  Future<T> callRequest() async {
    final Uri uri = Uri(
      scheme: 'https',
      host: ApiUrls.host,
      path: url,
    );
    printRequest(requestType: RequestType.put, uri: uri, param: param);
    final http.Response response = await http
        .put(
          uri,
          headers: header,
          body: jsonEncode(param),
        )
        .timeout(
          const Duration(seconds: 30),
        );
    printResponse(response);
    if (response.statusCode == 200) {
      return fromJson(response.body);
    } else {
      Exception exception = getException(statusCode: response.statusCode);
      throw (exception);
    }
  }
}
