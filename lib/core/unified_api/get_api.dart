import 'dart:async';
import 'dart:developer';
import '../constant/apis/apis_url.dart';
import 'printing.dart';
import 'package:http/http.dart' as http;
import 'initial_api.dart';

typedef _FromJson<T> = T Function(String body);

class GetApi<T> extends InitialApi<T> {
  GetApi({
    required String url,
    required this.fromJson,
    required String requestName,
    this.param,
  }) : super(requestName: requestName, url: url);

  _FromJson<T> fromJson;
  Map<String, dynamic>? param;

  @override
  Future<T> callRequest() async {
    final Uri uri = Uri(
      scheme: 'https',
      host: ApiUrls.host,
      path: url,
      queryParameters: param,
    );
    printRequest(requestType: RequestType.get, uri: uri);
    final http.Response response = await http
        .get(
          uri,
          headers: header,
        )
        .timeout(
          const Duration(seconds: 30),
        );
    printResponse(response);
    if (response.statusCode == 200) {
      return fromJson(response.body);
    } else {
      Exception exception = getException(statusCode: response.statusCode);
      throw (exception);
    }
  }
}
