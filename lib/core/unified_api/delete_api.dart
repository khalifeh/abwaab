import 'dart:async';

import 'package:http/http.dart' as http;

import '../constant/apis/apis_url.dart';
import 'initial_api.dart';
import 'printing.dart';

typedef _FromJson<T> = T Function(String body);

class DeleteApi<T> extends InitialApi<T> {
  DeleteApi({
    required String url,
    required this.fromJson,
    required String requestName,
  }) : super(requestName: requestName, url: url);

  _FromJson<T> fromJson;

  @override
  Future<T> callRequest() async {
    final Uri uri = Uri(
      scheme: 'https',
      host: ApiUrls.host,
      path: url,
    );
    printRequest(requestType: RequestType.delete, uri: uri);
    final http.Response response = await http
        .delete(
          uri,
          headers: header,
        )
        .timeout(
          const Duration(seconds: 30),
        );

    printResponse(response);
    if (response.statusCode == 200) {
      return fromJson(response.body);
    } else {
      Exception exception = getException(statusCode: response.statusCode);
      throw (exception);
    }
  }
}
