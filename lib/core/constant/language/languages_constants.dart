import 'package:flutter/cupertino.dart';

class LanguagesConstants {
  static const String englishLanguageCode = 'en';
  static const String turishLanguageCode = 'tr';

  static const Locale englishUsLocale = Locale('en', 'US');
  static const Locale turkishTurLocale = Locale('tr', 'TUR');
}
