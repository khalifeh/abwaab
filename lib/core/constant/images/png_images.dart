class PngImages {
  PngImages._();

  static const String _baseImagePath = "assets/images/";
  static const String onBoarding1 = _baseImagePath + "on_boarding_1.png";
  static const String onBoarding2 = _baseImagePath + "on_boarding_2.png";
  static const String onBoarding3 = _baseImagePath + "on_boarding_3.png";
  static const String checkEmail = '${_baseImagePath}check_email.png';
}
