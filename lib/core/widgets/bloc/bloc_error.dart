import 'package:flutter/material.dart';

enum BlocErrorState { serverError, timeLimite, userError, validationError, unkownError, noPosts }

// ignore: must_be_immutable
class BlocError extends StatelessWidget {
  Function? function;
  String? img;
  String errorTitle = "";
  IconData? iconData;
  factory BlocError({
    required BlocErrorState blocErrorState,
    required Function function,
    required BuildContext context,
    IconData? iconData,
    String img = "",
    String errorTitle = "",
  }) {
    if (blocErrorState == BlocErrorState.serverError) {
      return BlocError.serverError(function, context, iconData, errorTitle);
    } else if (blocErrorState == BlocErrorState.timeLimite) {
      return BlocError.timeLimite(function, context, iconData, errorTitle);
    } else if (blocErrorState == BlocErrorState.validationError) {
      return BlocError.validationError(function, context, iconData, errorTitle);
    } else if (blocErrorState == BlocErrorState.userError) {
      return BlocError.userError(function, context, iconData, errorTitle);
    } else if (blocErrorState == BlocErrorState.noPosts) {
      return BlocError.noPosts(function, context, iconData, errorTitle);
    } else {
      return BlocError.unkownError(function, context, iconData, errorTitle);
    }
  }

  BlocError.noPosts(
      Function passedFunction, BuildContext context, IconData? icon, String errorTitle,
      {Key? key})
      : super(key: key) {
    function = passedFunction;
    img = "assets/images/no_files_found.svg";
    this.errorTitle = (errorTitle.isNotEmpty) ? errorTitle : "there is not yet";
    iconData = icon;
  }

  BlocError.userError(
      Function passedFunction, BuildContext context, IconData? icon, String errorTitle,
      {Key? key})
      : super(key: key) {
    function = passedFunction;
    img = "assets/images/network_error.svg";
    this.errorTitle = (errorTitle.isNotEmpty) ? errorTitle : "check your internet connection";
    iconData = icon;
  }

  BlocError.validationError(
      Function passedFunction, BuildContext context, IconData? icon, String errorTitle,
      {Key? key})
      : super(key: key) {
    function = passedFunction;
    img = "assets/images/try_again.svg";
    this.errorTitle = (errorTitle.isNotEmpty) ? errorTitle : "check your internet connection";
    iconData = icon;
  }

  BlocError.unkownError(
      Function passedFunction, BuildContext context, IconData? icon, String errorTitle,
      {Key? key})
      : super(key: key) {
    function = passedFunction;
    img = "assets/images/try_again.svg";
    this.errorTitle = (errorTitle.isNotEmpty) ? errorTitle : "check your internet connection";
    iconData = icon;
  }

  BlocError.serverError(
      Function passedFunction, BuildContext context, IconData? icon, String errorTitle,
      {Key? key})
      : super(key: key) {
    function = passedFunction;
    img = "assets/images/network_error.svg";
    this.errorTitle = (errorTitle.isNotEmpty) ? errorTitle : "check your internet connection";
    iconData = icon;
  }

  BlocError.timeLimite(
      Function passedFunction, BuildContext context, IconData? icon, String errorTitle,
      {Key? key})
      : super(key: key) {
    function = passedFunction;
    img = "assets/images/network_error.svg";
    this.errorTitle = (errorTitle.isNotEmpty) ? errorTitle : "check your internet connection";
    iconData = icon;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SingleChildScrollView(
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: InkWell(
            onTap: () {
              function!();
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(iconData),
                const SizedBox(height: 12),
                Center(
                    child: Text(
                  errorTitle,
                ))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
