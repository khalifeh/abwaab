import 'package:flutter/cupertino.dart';

enum ScreenType { SMALL, MEDIUM, LARGE, XLARGE }

class ConfigScreen {
  double? screenWidth;
  double? screenHeight;
  ScreenType? screenType;
  ConfigScreen(BuildContext context) {
    this.screenWidth = MediaQuery.of(context).size.width;
    this.screenHeight = MediaQuery.of(context).size.height;
    _setScreen();
  }

  void _setScreen() {
    if (this.screenWidth! >= 320 && this.screenWidth! < 375)
      this.screenType = ScreenType.SMALL;
    else if (this.screenWidth! >= 375 && this.screenWidth! < 414)
      this.screenType = ScreenType.MEDIUM;
    else if (this.screenWidth! >= 414 && this.screenWidth! < 550)
      this.screenType = ScreenType.LARGE;
    else
      this.screenType = ScreenType.XLARGE;
  }
}

class WidgetSize {
  ConfigScreen? configScreen;
  WidgetSize(ConfigScreen configScreen) {
    this.configScreen = configScreen;
  }
}
