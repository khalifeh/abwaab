import 'package:flutter/material.dart';

class ColorsApp {
  static Color whiteColor = Colors.white;
  static Color primeryColor = primary;

  static const MaterialColor natural = MaterialColor(
    _naturalPrimaryValue,
    <int, Color>{
      50: Color(0xfffafafa),
      100: Colors.grey,
      200: Color(0xff616161),
      300: Color(0xffe0e0e0),
      500: Color(_naturalPrimaryValue),
      700: Color(0xff616161),
      900: Color(0xff000011),
    },
  );

  static const int _naturalPrimaryValue = 0xff9e9e9e;

  static const MaterialColor font = MaterialColor(
    _primaryPrimaryValue,
    <int, Color>{
      50: Color(0xfffafafa),
      100: Color(0xffBEBEBE),
      200: Color(0xffBFBFBF),
      300: Color(0xff6D6D6D),
      400: Color(0xff707070),
      500: Color(0xff9f9f9f),
    },
  );

  static const MaterialColor primary = MaterialColor(
    _primaryPrimaryValue,
    <int, Color>{
      100: Color(0xfffe893a),
      200: Color(0xffd97532),
      300: Color(0xffDD7842),
      400: Color(0xFFFF893B),
    },
  );

  static MaterialColor errors = const MaterialColor(
    _primaryPrimaryValue,
    <int, Color>{
      100: Color(0xffADD172),
      200: Color(0xffFF6969),
    },
  );
  static const int _primaryPrimaryValue = 0xfffe893a;
}
