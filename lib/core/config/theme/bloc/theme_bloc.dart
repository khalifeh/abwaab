import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';

import '../../color/colors_app.dart';

part 'theme_event.dart';
part 'theme_state.dart';

class ThemeBloc extends Bloc<ThemeEvent, ThemeState> {
  ThemeBloc() : super(ThemeState()) {
    on<ThemeEvent>((event, emit) {});
  }
}
