part of 'theme_bloc.dart';

class ThemeState {
  ThemeData themeData = ThemeData(
    primaryColor: ColorsApp.primary,
  );

  copyWith({Color? primeyColor}) {
    return ThemeData(
      primaryColor: primeyColor ?? themeData.primaryColor,
    );
  }
}
