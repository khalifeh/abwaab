import 'package:flutter/cupertino.dart';

class FontTextStyle {
  static TextStyle black({required double fontSize, required Color color}) {
    return TextStyle(
      fontSize: fontSize,
      color: color,
      fontFamily: "montserrat_black",
      height: 1.2,
    );
  }

  static TextStyle blackItalic({required double fontSize, required Color color}) {
    return TextStyle(
      fontSize: fontSize,
      color: color,
      fontFamily: "montserrat_blackItalic",
      height: 1.2,
    );
  }

  static TextStyle bold({required double fontSize, required Color color}) {
    return TextStyle(
      fontSize: fontSize,
      color: color,
      fontFamily: "montserrat_bold",
      height: 1.2,
    );
  }

  static TextStyle boldItalic({required double fontSize, required Color color}) {
    return TextStyle(
      fontSize: fontSize,
      color: color,
      fontFamily: "montserrat_boldItalic",
      height: 1.2,
    );
  }

  static TextStyle extraBold({required double fontSize, required Color color}) {
    return TextStyle(
      fontSize: fontSize,
      color: color,
      fontFamily: "montserrat_extrabold",
      height: 1.2,
    );
  }

  static TextStyle extraBoldItalic({required double fontSize, required Color color}) {
    return TextStyle(
      fontSize: fontSize,
      color: color,
      fontFamily: "montserrat_extraboldItalic",
      height: 1.2,
    );
  }

  static TextStyle light({required double fontSize, required Color color}) {
    return TextStyle(
      fontSize: fontSize,
      color: color,
      fontFamily: "montserrat_light",
      height: 1.2,
    );
  }

  static TextStyle extraLight({required double fontSize, required Color color}) {
    return TextStyle(
      fontSize: fontSize,
      color: color,
      fontFamily: "montserrat_extralightItalic",
      height: 1.2,
    );
  }

  static TextStyle extralightItalic({required double fontSize, required Color color}) {
    return TextStyle(
      fontSize: fontSize,
      color: color,
      fontFamily: "montserrat_black",
      height: 1.2,
    );
  }

  static TextStyle italic({required double fontSize, required Color color}) {
    return TextStyle(
      fontSize: fontSize,
      color: color,
      fontFamily: "montserrat_italic",
      height: 1.2,
    );
  }

  static TextStyle lightItalic({required double fontSize, required Color color}) {
    return TextStyle(
      fontSize: fontSize,
      color: color,
      fontFamily: "montserrat_lightItalic",
      height: 1.2,
    );
  }

  static TextStyle medium({required double fontSize, required Color color}) {
    return TextStyle(
      fontSize: fontSize,
      color: color,
      fontFamily: "montserrat_medium",
      height: 1.2,
    );
  }

  static TextStyle mediumItalic({required double fontSize, required Color color}) {
    return TextStyle(
      fontSize: fontSize,
      color: color,
      fontFamily: "montserrat_mediumItalic",
      height: 1.2,
    );
  }

  static TextStyle regular({required double fontSize, required Color color}) {
    return TextStyle(
      fontSize: fontSize,
      color: color,
      fontFamily: "montserrat_regular",
      height: 1.2,
    );
  }

  static TextStyle semiBold({required double fontSize, required Color color}) {
    return TextStyle(
      fontSize: fontSize,
      color: color,
      fontFamily: "montserrat_semibold",
      height: 1.2,
    );
  }

  static TextStyle semiBoldItalic({required double fontSize, required Color color}) {
    return TextStyle(
      fontSize: fontSize,
      color: color,
      fontFamily: "montserrat_semibolditalic",
      height: 1.2,
    );
  }

  static TextStyle thin({required double fontSize, required Color color}) {
    return TextStyle(
      fontSize: fontSize,
      color: color,
      fontFamily: "montserrat_thin",
      height: 1.2,
    );
  }

  static TextStyle thinItalic({required double fontSize, required Color color}) {
    return TextStyle(
      fontSize: fontSize,
      color: color,
      fontFamily: "montserrat_thinItalic",
      height: 1.2,
    );
  }
}
