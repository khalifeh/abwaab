import 'package:abwaab/feature/home_screen/presentation/service/status_color.dart';
import 'package:abwaab/feature/home_screen/presentation/widgets/true_check_circle.dart';
import 'package:flutter/material.dart';

class LearningPathCard extends StatelessWidget {
  final String name;
  final CheckOperationType checkOperationType;
  const LearningPathCard({
    Key? key,
    required this.name,
    required this.checkOperationType,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 60,
      child: Stack(
        children: [
          Card(
            child: Column(
              children: [
                Container(
                    color: checkOperationType == CheckOperationType.completed
                        ? StatusColor().completedColor
                        : StatusColor().uncompletedColor,
                    height: 4),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(child: Text(name, textAlign: TextAlign.center)),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 12),
            child: CheckOperationStyle(
              checkOperationType: checkOperationType == CheckOperationType.completed
                  ? CheckOperationType.completed
                  : CheckOperationType.noYet,
            ),
          )
        ],
      ),
    );
  }
}
