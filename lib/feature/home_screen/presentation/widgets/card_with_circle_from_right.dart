import 'package:abwaab/core/widgets/dash_painter/dashed_path_painter.dart';
import 'package:abwaab/feature/home_screen/presentation/service/status_color.dart';
import 'package:abwaab/feature/home_screen/presentation/widgets/learning_path_card.dart';
import '../../data/models/learning_path_model.dart';
import '../service/path_type.dart';
import 'package:flutter/material.dart';

class CardWithCircleFromRight extends StatelessWidget {
  final LearningPathModel learningPathModel;
  final bool isFinalItem;
  final bool isCurrentStateCompleted;
  final bool isNextStateCompleted;
  const CardWithCircleFromRight({
    Key? key,
    required this.learningPathModel,
    required this.isFinalItem,
    required this.isCurrentStateCompleted,
    required this.isNextStateCompleted,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        if (!isFinalItem)
          isCurrentStateCompleted
              ? CustomPaint(
                  willChange: true,
                  painter: DashedPathPainter(
                      originalPath: CustomePaintPathType(CustomeTypes.halfTopRight, context).path,
                      pathColor: StatusColor().completedColor,
                      dashGapLength: 0.1,
                      dashLength: 2,
                      strokeWidth: 2),
                  child: Row(),
                )
              : CustomPaint(
                  willChange: true,
                  painter: DashedPathPainter(
                      originalPath: CustomePaintPathType(CustomeTypes.halfTopRight, context).path,
                      pathColor: StatusColor().uncompletedColor),
                  child: Row(),
                ),
        if (!isFinalItem)
          isNextStateCompleted
              ? CustomPaint(
                  painter: DashedPathPainter(
                      originalPath:
                          CustomePaintPathType(CustomeTypes.halfBottomRight, context).path,
                      pathColor: StatusColor().completedColor,
                      dashGapLength: 0.1,
                      dashLength: 2,
                      strokeWidth: 2),
                  child: Row(),
                )
              : CustomPaint(
                  willChange: true,
                  painter: DashedPathPainter(
                      originalPath:
                          CustomePaintPathType(CustomeTypes.halfBottomRight, context).path,
                      pathColor: StatusColor().uncompletedColor),
                  child: Row(),
                ),
        Padding(
            padding: EdgeInsets.only(
              left: MediaQuery.of(context).size.width * 0.25,
              right: MediaQuery.of(context).size.width * 0.15,
            ),
            child: LearningPathCard(
              name: learningPathModel.name,
              checkOperationType:
                  isCurrentStateCompleted ? CheckOperationType.completed : CheckOperationType.noYet,
            )),
      ],
    );
  }
}
