import 'package:abwaab/core/config/color/colors_app.dart';
import 'package:abwaab/feature/home_screen/presentation/service/status_color.dart';
import 'package:flutter/material.dart';

class CheckOperationStyle extends StatelessWidget {
  final CheckOperationType checkOperationType;
  const CheckOperationStyle({Key? key, required this.checkOperationType}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (CheckOperationType.completed == checkOperationType) {
      return CircleAvatar(
        maxRadius: 15,
        backgroundColor: StatusColor().completedColor,
        child: Icon(
          Icons.check,
          color: ColorsApp.natural.shade50,
          size: 28,
        ),
      );
    }
    return CircleAvatar(
      maxRadius: 15,
      backgroundColor: StatusColor().uncompletedColor,
      child: Icon(
        Icons.info_outline,
        color: ColorsApp.natural.shade50,
        size: 28,
      ),
    );
  }
}
