import 'package:abwaab/feature/home_screen/data/models/learning_path_model.dart';
import 'package:flutter/material.dart';

import 'card_with_circle_from_left.dart';
import 'card_with_circle_from_right.dart';

class HomeScreenBody extends StatelessWidget {
  final List<LearningPathModel> learningPaths;
  const HomeScreenBody({Key? key, required this.learningPaths}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      separatorBuilder: (context, index) {
        return const SizedBox(height: 12);
      },
      itemBuilder: (contex, index) {
        if (index % 2 == 0) {
          return CardWithCircleFromLeft(
              learningPathModel: learningPaths[index],
              isFinalItem: index == learningPaths.length - 1,
              isCurrentStateCompleted: !(index == learningPaths.length - 1),
              isNextStateCompleted: !(index + 1 == learningPaths.length - 1));
        } else {
          return CardWithCircleFromRight(
              learningPathModel: learningPaths[index],
              isFinalItem: index == learningPaths.length - 1,
              isCurrentStateCompleted: !(index == learningPaths.length - 1),
              isNextStateCompleted: !(index + 1 == learningPaths.length - 1));
        }
      },
      itemCount: learningPaths.length,
    );
  }
}
