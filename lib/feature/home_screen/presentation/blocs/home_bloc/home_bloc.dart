import 'dart:async';

import '../../../../../core/error/failures.dart';
import '../../../data/models/home_screen_learning_path_model.dart';
import '../../../data/models/learning_path_model.dart';
import '../../../data/repositroy/home_screen_repositories_impl.dart';
import '../../../domain/use_case/get_learning_path.dart';
import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final EmailActivationUseCase _homeScreenRepositories =
      EmailActivationUseCase(homeScreenRepositories: HomeScreenRepositoriesImpl());
  HomeBloc() : super(HomeInitial()) {
    on<GetLearningPath>(_mapGetLearningPath);
  }

  FutureOr<void> _mapGetLearningPath(GetLearningPath event, Emitter<HomeState> emit) async {
    emit(LoadingState());
    Either<Failure, HomeScreenLearningPathModel> result =
        await _homeScreenRepositories.call(LearningPathParam());
    result.fold((l) {
      emit(FailedGetLearningPath());
    }, (r) {
      emit(SuccessGetLearningPath(learningPaths: r.learningPaths));
    });
  }
}
