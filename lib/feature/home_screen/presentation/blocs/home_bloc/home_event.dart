part of 'home_bloc.dart';

@immutable
abstract class HomeEvent {}

class GetLearningPath extends HomeEvent {}
