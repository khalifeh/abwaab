part of 'home_bloc.dart';

@immutable
abstract class HomeState {}

class HomeInitial extends HomeState {}

class LoadingState extends HomeState {}

class SuccessGetLearningPath extends HomeState {
  final List<LearningPathModel> learningPaths;
  SuccessGetLearningPath({required this.learningPaths});
}

class FailedGetLearningPath extends HomeState {}
