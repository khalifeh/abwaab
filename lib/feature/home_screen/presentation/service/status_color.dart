import 'package:abwaab/core/config/color/colors_app.dart';
import 'package:flutter/material.dart';

enum CheckOperationType { completed, noYet }

class StatusColor {
  late Color completedColor;
  late Color uncompletedColor;
  StatusColor() {
    completedColor = ColorsApp.primary;
    uncompletedColor = ColorsApp.natural.shade500;
  }
}
