import 'package:flutter/cupertino.dart';
import 'dart:math' as math;

import 'package:flutter/material.dart';

enum CustomeTypes {
  fullFillLeft,
  fullFillRight,
  halfBottomLeft,
  halfTopLeft,
  halfBottomRight,
  halfTopRight,
}

const double _bottom = 100;
const double _top = 24;
const double _padding = 10;

class CustomePaintPathType {
  late Path path;
  late double width;
  factory CustomePaintPathType(CustomeTypes type, BuildContext context) {
    if (type == CustomeTypes.fullFillLeft) {
      return CustomePaintPathType.fullFillLeft(context);
    } else if (type == CustomeTypes.fullFillRight) {
      return CustomePaintPathType.fullFillRight(context);
    } else if (type == CustomeTypes.halfBottomLeft) {
      return CustomePaintPathType.halfBottomLeft(context);
    } else if (type == CustomeTypes.halfTopLeft) {
      return CustomePaintPathType.halfTopLeft(context);
    } else if (type == CustomeTypes.halfTopRight) {
      return CustomePaintPathType.halfTopRight(context);
    }
    return CustomePaintPathType.halfBottomRight(context);
  }

  CustomePaintPathType.halfBottomRight(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    final rect = Rect.fromLTRB(width * 0.56, _top, width - _padding, _bottom);
    const startAngle = (-math.pi / 2) - 0.1;
    path = Path();
    path.moveTo(width * 0.78, _top);
    path.lineTo(width * 0.79, _top);
    path.addArc(rect, 0.31, 1.7);
    path.moveTo(width * 0.78, _bottom);
    path.lineTo(width * 0.65, _bottom);
  }

  CustomePaintPathType.halfTopRight(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    final rect = Rect.fromLTRB(width * 0.6, _top, width - _padding, _bottom);
    const startAngle = (-math.pi / 2) - 0.1;
    const sweepAngle = 2.2;
    path = Path();
    path.moveTo(width * 0.78, _top);
    path.addArc(rect, startAngle, sweepAngle);
  }

  CustomePaintPathType.halfTopLeft(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    path = Path();
    final rect = Rect.fromLTRB(_padding, _top, width * 0.3, _bottom);
    path.addArc(rect, 2.64, 2);
    path.moveTo(width * 0.78, _top);
  }

  CustomePaintPathType.halfBottomLeft(BuildContext context) {
    path = Path();
    final rect = Rect.fromLTRB(_padding, _top, MediaQuery.of(context).size.width * 0.3, _bottom);
    const startAngle = (math.pi / 2) - 0.1;
    path.addArc(rect, startAngle, 1.2);
    path.moveTo(MediaQuery.of(context).size.width * 0.15, _bottom);
    path.lineTo(MediaQuery.of(context).size.width * 0.3, _bottom);
  }

  CustomePaintPathType.fullFillRight(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    final rect = Rect.fromLTRB(width * 0.56, _top, width - _padding, _bottom);
    const startAngle = (-math.pi / 2) - 0.1;
    const sweepAngle = math.pi + 0.2;
    path = Path();
    path.moveTo(width * 0.78, _top);
    path.lineTo(width * 0.79, _top);
    path.addArc(rect, startAngle, sweepAngle);
    path.moveTo(width * 0.78, _bottom);
    path.lineTo(width * 0.65, _bottom);
  }

  CustomePaintPathType.fullFillLeft(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    final rect = Rect.fromLTRB(_padding, _top, width * 0.3, _bottom);
    const startAngle = (math.pi / 2) - 0.1;
    const sweepAngle = math.pi + 0.2;
    path = Path();
    path.addArc(rect, startAngle, sweepAngle);
    path.moveTo(width * 0.15, _bottom);
    path.lineTo(width * 0.3, _bottom);
  }
}
