import 'package:abwaab/feature/home_screen/presentation/widgets/home_screen_body.dart';
import '../../../../core/widgets/bloc/bloc_error.dart';
import '../../../../core/widgets/bloc/loading_bloc.dart';
import '../blocs/home_bloc/home_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late HomeBloc homeBloc;
  @override
  void initState() {
    super.initState();
    homeBloc = HomeBloc();
    homeBloc.add(GetLearningPath());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider<HomeBloc>(
        create: (context) => homeBloc,
        child: BlocBuilder<HomeBloc, HomeState>(
          builder: (context, state) {
            if (state is LoadingState) {
              return const LoadingBloc();
            } else if (state is FailedGetLearningPath) {
              return BlocError(
                  blocErrorState: BlocErrorState.serverError,
                  context: context,
                  function: () {
                    homeBloc.add(GetLearningPath());
                  },
                  iconData: Icons.replay_outlined);
            }
            if (state is SuccessGetLearningPath) {
              return HomeScreenBody(learningPaths: state.learningPaths);
            } else {
              return const LoadingBloc();
            }
          },
        ),
      ),
    );
  }
}
