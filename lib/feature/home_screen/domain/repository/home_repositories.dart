import '../../../../core/error/failures.dart';
import '../../data/models/home_screen_learning_path_model.dart';
import 'package:dartz/dartz.dart';

abstract class HomeScreenRepositories {
  Future<Either<Failure, HomeScreenLearningPathModel>> getLearningPath();
}
