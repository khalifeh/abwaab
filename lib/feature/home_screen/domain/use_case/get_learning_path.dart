import '../../data/models/home_screen_learning_path_model.dart';
import '../repository/home_repositories.dart';

import '../../../../core/use_case/use_case.dart';
import 'package:dartz/dartz.dart';
import '../../../../core/error/failures.dart';

class EmailActivationUseCase implements UseCase<HomeScreenLearningPathModel, LearningPathParam> {
  EmailActivationUseCase({required this.homeScreenRepositories});

  final HomeScreenRepositories homeScreenRepositories;

  @override
  Future<Either<Failure, HomeScreenLearningPathModel>> call(LearningPathParam params) async {
    return await homeScreenRepositories.getLearningPath();
  }
}

class LearningPathParam {
  LearningPathParam();
}
