import 'address_model.dart';
import 'company_model.dart';
import 'package:json_annotation/json_annotation.dart';
part 'learning_path_model.g.dart';

@JsonSerializable()
class LearningPathModel {
  @JsonKey(name: 'id')
  final int id;
  @JsonKey(name: 'name')
  final String name;
  @JsonKey(name: 'username')
  final String username;
  @JsonKey(name: 'email')
  final String email;
  @JsonKey(name: 'address')
  final AddressModel address;
  @JsonKey(name: 'phone')
  final String phone;
  @JsonKey(name: 'website')
  final String website;
  @JsonKey(name: 'company')
  final CompanyModel company;

  LearningPathModel({
    required this.id,
    required this.name,
    required this.username,
    required this.email,
    required this.address,
    required this.phone,
    required this.website,
    required this.company,
  });
  factory LearningPathModel.fromJson(Map<String, dynamic> json) {
    return _$LearningPathModelFromJson(json);
  }
  Map<String, dynamic> toJson() {
    return _$LearningPathModelToJson(this);
  }
}
