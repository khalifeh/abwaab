import 'package:json_annotation/json_annotation.dart';
part 'company_model.g.dart';

@JsonSerializable()
class CompanyModel {
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'catchPhrase')
  String? catchPhrase;
  @JsonKey(name: 'bs')
  String? bs;

  CompanyModel({this.name, this.catchPhrase, this.bs});
  factory CompanyModel.fromJson(Map<String, dynamic> json) {
    return _$CompanyModelFromJson(json);
  }
  Map<String, dynamic> toJson() {
    return _$CompanyModelToJson(this);
  }
}
