import 'learning_path_model.dart';
import 'dart:convert';

class HomeScreenLearningPathModel {
  final List<LearningPathModel> learningPaths;
  HomeScreenLearningPathModel({required this.learningPaths});

  factory HomeScreenLearningPathModel.fromJson(String json) {
    List<LearningPathModel> learningPathModel = [];
    jsonDecode(json).forEach((element) {
      Map<String, dynamic> map = element as Map<String, dynamic>;
      learningPathModel.add(LearningPathModel.fromJson(map));
    });
    return HomeScreenLearningPathModel(
      learningPaths: learningPathModel,
    );
  }
}
