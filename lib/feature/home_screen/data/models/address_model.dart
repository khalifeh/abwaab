import 'geo_model.dart';
import 'package:json_annotation/json_annotation.dart';
part 'address_model.g.dart';

@JsonSerializable()
class AddressModel {
  @JsonKey(name: 'street')
  final String street;
  @JsonKey(name: 'suite')
  final String suite;
  @JsonKey(name: 'city')
  final String city;
  @JsonKey(name: 'zipcode')
  final String zipcode;
  @JsonKey(name: 'geo')
  final GeoModel geo;

  AddressModel({
    required this.street,
    required this.suite,
    required this.city,
    required this.zipcode,
    required this.geo,
  });
  factory AddressModel.fromJson(Map<String, dynamic> json) {
    return _$AddressModelFromJson(json);
  }
  Map<String, dynamic> toJson() {
    return _$AddressModelToJson(this);
  }
}
