import 'package:json_annotation/json_annotation.dart';
part 'geo_model.g.dart';

@JsonSerializable()
class GeoModel {
  @JsonKey(name: 'lat')
  final String lat;
  @JsonKey(name: 'lng')
  final String lng;

  GeoModel({required this.lat, required this.lng});

  factory GeoModel.fromJson(Map<String, dynamic> json) {
    return _$GeoModelFromJson(json);
  }
  Map<String, dynamic> toJson() {
    return _$GeoModelToJson(this);
  }
}
