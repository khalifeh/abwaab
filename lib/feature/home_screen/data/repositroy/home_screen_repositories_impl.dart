import '../../../../core/unified_api/handling_exception.dart';
import '../data_soure/get_learning_path.dart';
import '../models/home_screen_learning_path_model.dart';
import '../../../../core/error/failures.dart';
import '../../domain/repository/home_repositories.dart';
import 'package:dartz/dartz.dart';

class HomeScreenRepositoriesImpl implements HomeScreenRepositories {
  @override
  Future<Either<Failure, HomeScreenLearningPathModel>> getLearningPath() async {
    return await HandlingExceptionManager.wrapHandling<HomeScreenLearningPathModel>(tryCall: () async {
      final model = await GetLearningPath().getLearningPath();
      return Right(model);
    });
  }
}
