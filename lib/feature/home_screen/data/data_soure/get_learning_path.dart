import '../../../../core/unified_api/get_api.dart';
import '../models/home_screen_learning_path_model.dart';
import '../../../../core/constant/apis/apis_url.dart';

class GetLearningPath {
  Future<HomeScreenLearningPathModel> getLearningPath() async {
    GetApi<HomeScreenLearningPathModel> postApi = GetApi<HomeScreenLearningPathModel>(
      fromJson: (jsonString) {
        return HomeScreenLearningPathModel.fromJson(jsonString);
      },
      url: ApiUrls.learningPath,
      requestName: "get Learning Path",
    );
    return await postApi.callRequest();
  }
}
